#include <LiquidCrystal.h> 

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(12,11,10,9,8,7,6,5,4,3,2);      

void setup()
{
  lcd.begin(16,2);      // set up the LCD's number of columns and rows:
  lcd.setCursor(0,0);   // set the cursor to column 0, line 0
  lcd.print("Welcome"); //// Print a message to the LCD.
  lcd.setCursor(0,2);   // set the cursor to column 0, line 2
  lcd.print("Arduino is Easy");  //Print a message to the LCD.
}
void loop()
{}
// http://wiki.epalsite.com/index.php?title=Starter_Kit_for_Arduino#Lesson_19_1602LCD