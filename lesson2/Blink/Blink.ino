/*
TEX
BLINK
20190929
*/
void setup() {
  // put your setup code here, to run once:
  
  Serial.begin(9600);
  Serial.println("Welcome to Arduino");
  
  // initialize the digital pin as an output.
  pinMode(13, OUTPUT);  // built-in LED
}

void loop() {
  // put your main code here, to run repeatedly:
  
  digitalWrite(13, HIGH);                // set the LED on
  Serial.println("LED on - wait 250");
  delay(250);                            // wait for a quarter of a second
  digitalWrite(13, LOW);                 // set the LED off
  Serial.println("LED off - wait 250");
  delay(250);                            // wait for a quarter of a second
}

//http://wiki.epalsite.com/index.php?title=Starter_Kit_for_Arduino#Lesson_2_LED_Blink
