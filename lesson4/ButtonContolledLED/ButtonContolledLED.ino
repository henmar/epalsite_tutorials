/*
TEX
Lesson 4
Button Controlled LED
Turns on LED when button is pushed
20180125
*/
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println("Welcome to Arduino");
  pinMode(2, INPUT_PULLUP);  // initialize the digital pin as an input.
  pinMode(5, OUTPUT);        // initialize the analog/pwm pin as output
}

void loop() {
  // put your main code here, to run repeatedly:
  int sensorValue = digitalRead(2);
  if (sensorValue){
    digitalWrite(5, LOW); // set the LED off
    Serial.println("LED on");
  } else {
    digitalWrite(5, HIGH);  // set the LED on
    Serial.println("LED off");
  }
  
}

//http://wiki.epalsite.com/index.php?title=Starter_Kit_for_Arduino#Lesson_1_Welcome_to_Arduino
