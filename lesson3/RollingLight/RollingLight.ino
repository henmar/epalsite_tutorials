/*
  Lesson 3
  Rolling light
  Turns on one LED on for 100 milliseconds, then roll to the next one.
  http://wiki.epalsite.com/index.php?title=Starter_Kit_for_Arduino
*/
int i;
void setup() {     
  // put your setup code here, to run once:
  Serial.begin(9600);           
  // initialize the digital pins 2 - 9  as output.
  for(i = 2; i < 10; i++) {
    pinMode(i, OUTPUT);  
  }   
}

void loop() {
  for(i = 2; i < 10; i++) {
    digitalWrite(i, HIGH);   // set the LED on
    String output1 = String("LED");
    String output2 = String(" on - wait 100");
    Serial.println(output1 + i + output2);
    delay(100);              // wait for a second
    digitalWrite(i, LOW);    // set the LED off
  }
}
//http://wiki.epalsite.com/index.php?title=Starter_Kit_for_Arduino
